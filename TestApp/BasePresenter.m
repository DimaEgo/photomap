//
//  BasePresenter.m
//  TestApp
//
//  Created by mac-061 on 3/14/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BasePresenter.h"

@implementation BasePresenter : NSObject

- (instancetype)initWithView:(id)view{
    
    if (self = [super init]) {
        _view = view;
    }
    return self;
}

- (void) attachView:(id)view {
    _view = view;
}

- (void)detachView{
    _view = nil;
}
@end
