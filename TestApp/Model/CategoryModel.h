//
//  Category.h
//  TestApp
//
//  Created by mac-061 on 3/15/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryModel : NSObject

@property (nonatomic,assign)NSString *title; 
@property (nonatomic,assign)BOOL     isSelected;
@property (nonatomic,assign)NSString *color;

- (id)initWithParams:(NSString *)title setSelected:(BOOL)isSelected setColor:(NSString *) color;

@end
