//
//  Section.h
//  TestApp
//
//  Created by L on 3/18/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Section : NSObject

@property (nonatomic,strong)NSString   *title;
@property (nonatomic,assign)NSInteger   month;
@property (nonatomic,assign)NSInteger   year;

@property (nonatomic,strong)NSMutableArray *objects;

@end
