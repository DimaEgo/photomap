//
//  Category.m
//  TestApp
//
//  Created by mac-061 on 3/15/18.
//  Copyright © 2018 mac-061. All rights reserved.
//


#import "CategoryModel.h"

@implementation CategoryModel

- (id)initWithParams:(NSString *)title setSelected:(BOOL) isSelected setColor:(NSString *) color {
    if( self = [super init] ){
        self.title = title;
        self.isSelected = isSelected;
        self.color = color;
    }
    
    return self;
}

@end
