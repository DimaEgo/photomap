//
//  Header.h
//  TestApp
//
//  Created by mac-061 on 3/16/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PhotoModel : NSObject

@property (nonatomic,assign)NSString   *title;
@property (nonatomic,readwrite)NSDate  *date;
@property (nonatomic,assign)NSString   *category;
@property (nonatomic,assign)float       latitude;
@property (nonatomic,assign)float       longitude;
@property (nonatomic,readwrite)UIImage *photo;

@end
