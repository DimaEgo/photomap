//
//  MarkerView.h
//  TestApp
//
//  Created by mac-061 on 3/14/18.
//  Copyright © 2018 mac-061. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <MapKit/Mapkit.h>

@interface PhotoAnnotaionView :  MKAnnotationView

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation;
@end
