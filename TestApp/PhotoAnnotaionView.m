//
//  MarkerView.m
//  TestApp
//
//  Created by mac-061 on 3/14/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoAnnotaionView.h"


@implementation PhotoAnnotaionView

@synthesize coordinate;

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation {
    self = [super initWithAnnotation:annotation reuseIdentifier:nil];
    
    
    self.canShowCallout = YES;
    self.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    self.image = [UIImage imageNamed:@"ic_camera"];
    
    return self;
}


@end
