//
//  BasePresenter.h
//  TestApp
//
//  Created by mac-061 on 3/14/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BasePresenter<E>: NSObject{
    __weak E _view;
}

- (instancetype) initWithView:(E)view;

- (void) attachView:(E)view ;

- (void)detachView;
@end
