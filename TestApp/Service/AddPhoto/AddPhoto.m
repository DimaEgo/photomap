//
//  AddPhoto.m
//  TestApp
//
//  Created by mac-061 on 3/20/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AddPhoto.h"
#import "PhotoModel.h"
#import "PhotoRepository.h"

@implementation AddPhoto 

-(void) execute: (PhotoModel *) photo {
    
    PhotoRepository *repository = [PhotoRepository sharedManager];
    [repository addPhoto:photo];
    
}
@end 
