//
//  GetAllPhotos.m
//  TestApp
//
//  Created by mac-061 on 3/16/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import "GetAllPhotos.h"
#import "PhotoModel.h"

@implementation GetAllPhotos

- (NSArray *)execute {
    
    NSMutableArray *photos = [NSMutableArray new];
    
    for (int i=0; i<10; i++) {
        PhotoModel * photo = [PhotoModel new];
        
        photo.title     = @"Photo";
        photo.date      = [NSDate new];
        photo.photo     = nil;
        photo.latitude  = 50.0 + i;
        photo.longitude = 100.0 + i;
        photo.category = @"Friends";
        
        [photos addObject:(photo)];
    }
    
    return photos;
}

@end
