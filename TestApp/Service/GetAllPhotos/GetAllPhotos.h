//
//  GetAllPhotos.h
//  TestApp
//
//  Created by mac-061 on 3/16/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetAllPhotos: NSObject

- (NSArray*) execute;

@end
