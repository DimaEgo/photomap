//
//  GetCategories.m
//  TestApp
//
//  Created by mac-061 on 3/16/18.
//  Copyright © 2018 mac-061. All rights reserved.
//


#import "GetCategories.h"
#import "CategoryModel.h"
#import "PhotoRepository.h"
#import "PhotoRepository.h"


@implementation GetCategories

- (void)execute: (void(^)(NSMutableArray *categories)) completion{
   
    PhotoRepository *repository = [PhotoRepository sharedManager];
   return [repository categories:completion];
}

@end
