//
//  GetCategories.h
//  TestApp
//
//  Created by mac-061 on 3/15/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetCategories: NSObject

- (void)execute: (void(^)(NSMutableArray *categories)) completion;

@end
