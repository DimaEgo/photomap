//
//  UpdateCategories.h
//  TestApp
//
//  Created by mac-061 on 3/26/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhotoModel.h"

@interface UpdateCategories : NSObject

-(void) execute: (NSArray *) categories;

@end
