//
//  UpdateCategories.m
//  TestApp
//
//  Created by mac-061 on 3/26/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AddPhoto.h"
#import "UpdateCategories.h"
#import "PhotoRepository.h"

@implementation UpdateCategories

-(void) execute: (NSArray *) categories {
    
    PhotoRepository *repository = [PhotoRepository sharedManager];
    [repository updateCategories:categories];
}
@end
