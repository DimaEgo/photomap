//
//  MapPresenter.h
//  TestApp
//
//  Created by mac-061 on 3/14/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import "MapViewProtocol.h"
#import "BasePresenter.h"
#import <MapKit/Mapkit.h>

@interface MapPresenter : BasePresenter<id<MapViewProtocol>>

- (void)buttonCurrentLocationClicked;

- (void)buttonChoosePhotoClicked;

- (void)buttonCategoriesClicked;

- (void)mapLongClicked: (CGPoint *) point;

- (void)imageSelected:(UIImage *) image withDate:(NSDate *) date;

- (void)addPhotoConfirmed:(PhotoModel *) image;
@end
