//
//  MapPresenter.m
//  TestApp
//
//  Created by mac-061 on 3/14/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import "MapPresenter.h"
#import <MapKit/Mapkit.h>
#import "PhotoModel.h"
#import "AddPhoto.h"

@interface MapPresenter()

@property (nonatomic, strong) PhotoModel *photoModel;

@end

@implementation MapPresenter

- (void)buttonCurrentLocationClicked{
    [_view showCurrentLocationOnMap];
}

- (void)buttonChoosePhotoClicked{
    self.photoModel = [PhotoModel new];
    [_view showPhotoChooser];
}

- (void)buttonCategoriesClicked{
    
}

-(void)mapLongClicked: (CGPoint*) point{
    self.photoModel = [PhotoModel new];
    [_view showPhotoChooser];
}

- (void)imageSelected:(UIImage *) image withDate:(NSDate *) date{
    self.photoModel.photo = image;
    self.photoModel.date  = date;
    [_view showAddNewPhotoPopover: self.photoModel];
}

-(void)addPhotoConfirmed:(PhotoModel *)photo{
    AddPhoto *addPhoto = [AddPhoto new];
    [addPhoto execute:photo];
}

@end
