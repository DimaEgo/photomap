//
//  TabViewController.h
//  TestApp
//
//  Created by mac-061 on 3/16/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabViewController: UITabBarController

@property (strong, nonatomic) NSArray *photos;

@end
