//
//  TabViewController.m
//  TestApp
//
//  Created by mac-061 on 3/16/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TabViewController.h"

#import "GetAllPhotos.h"

@implementation TabViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    GetAllPhotos * getAllPhotos = [GetAllPhotos new];
    _photos = getAllPhotos.execute;
    
}

-(void)viewWillAppear:(BOOL)animated {
    
}

-(void)viewWillDisappear:(BOOL)animated {
    
}

-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end
