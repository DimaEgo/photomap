//
//  FullPhotoViewController.m
//  TestApp
//
//  Created by mac-061 on 3/19/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FullPhotoViewController.h"
#import <UIKit/UIKit.h>

@interface FullPhotoViewController()

@property (weak, nonatomic) IBOutlet UIImageView *imagePhoto;
@property (weak, nonatomic) IBOutlet UILabel     *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel     *labelDescription;

@end

@implementation FullPhotoViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.labelTitle.text  = _photoModel.title;
    self.imagePhoto.image = _photoModel.photo;
}

-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end
