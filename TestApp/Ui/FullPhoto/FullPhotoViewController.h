//
//  FullPhotoViewController.h
//  TestApp
//
//  Created by mac-061 on 3/19/18.
//  Copyright © 2018 mac-061. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "PhotoModel.h"

@interface FullPhotoViewController: UIViewController

@property (strong, nonatomic) PhotoModel *photoModel;

@end
