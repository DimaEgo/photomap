//
//  AddPhotoViewController.m
//  TestApp
//
//  Created by mac-061 on 3/20/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddPhotoViewController.h"
#import "FullPhotoViewController.h"
#import <UIKit/UIKit.h>
#import "GetCategories.h"
#import "CategoryModel.h"

@interface AddPhotoViewController()<UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *categoryImage;

@property (weak, nonatomic) IBOutlet UIImageView *photoImage;
@property (weak, nonatomic) IBOutlet UILabel     *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UITextField *describtionText;

@property (strong, nonatomic) NSDateFormatter *uiDateFormat;

@property (strong, nonatomic) NSArray *categories;

@end

@implementation AddPhotoViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    UITapGestureRecognizer *photoGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClicked)];
    [self.photoImage setUserInteractionEnabled:YES];
    [self.photoImage addGestureRecognizer:photoGestureRecognizer];
    
    self.uiDateFormat = [NSDateFormatter new];
    [self.uiDateFormat setDateStyle: NSDateFormatterLongStyle];
    [self.uiDateFormat setTimeStyle: NSDateFormatterShortStyle];
    
    self.dateLabel.text = [self.uiDateFormat stringFromDate: _photoModel.date];
    self.photoImage.image = _photoModel.photo;
    
    GetCategories *getCategories = [GetCategories new];
//    self.categories = getCategories.execute;
    
    UITapGestureRecognizer *categoryGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(categoryClicked)];
    [self.categoryLabel setUserInteractionEnabled:YES];
    [self.categoryLabel addGestureRecognizer:categoryGestureRecognizer];
}

-(void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

- (void)imageClicked {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    FullPhotoViewController *fullPhotoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"FullPhoto"];
    fullPhotoController.photoModel = _photoModel;
    
    [self presentViewController:fullPhotoController animated:YES completion:nil];
}

-(void)categoryClicked {
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//
//    CGRect pickerFrame = CGRectMake(0, 0, 270, 270);
//    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:pickerFrame];
//    picker.delegate = self;
//    [alert.view addSubview:picker];
//
//    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Select" style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {
//                                                              //  [action doSomething];
//                                                          }];
//    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
//                                                   handler:^(UIAlertAction * action) {
//                                                       [alert dismissViewControllerAnimated:YES completion:nil];
//                                                   }];
//
//
//    [alert addAction:defaultAction];
//    [alert addAction:cancel];
//
//
//
//
//
//    [self presentViewController:alert animated:YES completion:nil];
//
//
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    CGFloat margin = 8.0F;
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(margin, margin, alertController.view.bounds.size.width - margin, 100.0F)];
    picker.delegate = self;
    [alertController.view addSubview:picker];
    
    UIAlertAction *somethingAction = [UIAlertAction actionWithTitle:@"Select" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {}];
    [alertController addAction:somethingAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{}];
}

- (IBAction)doneButtonClicked:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:^{
        if (_addPhotoDelegate) {
            [_addPhotoDelegate addPhotoConfirmed:_photoModel];
        }
    }];
}

- (IBAction)cancelButtonClicked:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_categories count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return ((CategoryModel *)[_categories objectAtIndex: row]).title;
}

@end
