//
//  AddPhotoDelegate.h
//  TestApp
//
//  Created by mac-061 on 3/21/18.
//  Copyright © 2018 mac-061. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "PhotoModel.h"

@protocol AddPhotoDelegate <NSObject>

-(void) addPhotoConfirmed: (PhotoModel*) photo;

@end
