//
//  AddPhotoViewController.h
//  TestApp
//
//  Created by mac-061 on 3/20/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoModel.h"
#import "AddPhotoDelegate.h"


@interface AddPhotoViewController: UIViewController

@property (strong, nonatomic) PhotoModel *photoModel;
@property (weak, nonatomic) id <AddPhotoDelegate> addPhotoDelegate;

@end
