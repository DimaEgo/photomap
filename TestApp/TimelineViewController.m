//
//  TimelineViewController.m
//  TestApp
//
//  Created by mac-061 on 3/15/18.
//  Copyright © 2018 mac-061. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "TimelineViewController.h"
#import "FullPhotoViewController.h"
#import "TabViewController.h"
#import "TimelineCell.h"
#import <MapKit/MapKit.h>
#import "PhotoModel.h"
#import "Section.h"

@interface TimelineViewController()

@property (weak, nonatomic) IBOutlet UITableView *timelineTable;

@property (strong, nonatomic) NSDateFormatter *cellDateFormat;
@property (strong, nonatomic) NSDateFormatter *cellHeaderDateFormat;

@property (strong, nonatomic) NSMutableArray *sections;

@end

@implementation TimelineViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    [self initDateFormatters];
    
    self.timelineTable.dataSource = self;
    self.timelineTable.delegate   = self;
    [self.timelineTable registerNib:[UINib nibWithNibName:@"TimelineCell" bundle:nil] forCellReuseIdentifier:@"TimelineCell"];
    
    [self getPhotos];
}

- (void) initDateFormatters{
    
    self.cellDateFormat = [NSDateFormatter new];
    [self.cellDateFormat setDateFormat:@"dd-MM-yy"];
    
    self.cellHeaderDateFormat = [NSDateFormatter new];
    [self.cellHeaderDateFormat setDateFormat:@"MMMM yyyy"];
}

- (void)getPhotos {
    TabViewController *controller = ((TabViewController *) [self tabBarController]);
    
    _photos = [controller.photos sortedArrayUsingComparator: ^NSComparisonResult(PhotoModel *p1, PhotoModel *p2){
        return [p1.date compare: p2.date];
    }];
    
    self.sections = [NSMutableArray new];
    
    Section *currentSection = [Section new];
    
    for (int i=0; i<_photos.count; i++) {
        
        PhotoModel *photo = ((PhotoModel *)[_photos objectAtIndex:i]);
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:photo.date];
        
        NSInteger month = [components month];
        NSInteger year  = [components year];
        
        if (month != currentSection.month && year != currentSection.year) {
            currentSection = [Section new];
            currentSection.title = [self.cellHeaderDateFormat stringFromDate: photo.date];
            currentSection.year = year;
            currentSection.month = month;
            currentSection.objects = [[NSMutableArray alloc] init];
            [self.sections addObject:currentSection];
        }
        
        [currentSection.objects addObject:photo];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [((Section* )[self.sections objectAtIndex:section]).objects count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return ((Section* )[self.sections objectAtIndex:section]).title;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.sections count];
}

- (UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath {
    TimelineCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TimelineCell"];
    
    Section *section = ((Section *)[self.sections objectAtIndex:indexPath.section]);
    
    PhotoModel *photo = ((PhotoModel *)[section.objects objectAtIndex:indexPath.row]);
    
    NSString *description = [NSString new];
    description = [description stringByAppendingString: [self.cellDateFormat stringFromDate: photo.date]];
    description = [description stringByAppendingString: @" / "];
    description = [description stringByAppendingString: [photo.category uppercaseString]];
    
    cell.labelDescription.text = description;
    cell.labelTitle.text = photo.title;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"FullPhoto" sender:tableView ];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"FullPhoto"]) {
        FullPhotoViewController *fullPhotoViewController = (FullPhotoViewController *)segue.destinationViewController;
        
        PhotoModel *selectedPhoto = ((PhotoModel *)[_photos objectAtIndex:[self.timelineTable indexPathForSelectedRow].row]);
        
        fullPhotoViewController.photoModel = selectedPhoto;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
