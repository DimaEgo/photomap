//
//  CategoryCell.h
//  TestApp
//
//  Created by mac-061 on 3/15/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckboxButton.h"

@interface CategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet CheckboxButton *checkboxCategory;
@property (weak, nonatomic) IBOutlet UILabel        *labelCategoryTitle;

@end
