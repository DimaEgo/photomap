//
//  PhotoAnnotation.m
//  TestApp
//
//  Created by L on 3/17/18.
//  Copyright © 2018 mac-061. All rights reserved.
//


#import "PhotoAnnotation.h"
#import "UIColor+PXExtensions.h"

@implementation PhotoAnnotation

- (id)initWithTitle: (NSString *)title andWithLocation: (CLLocationCoordinate2D) location{
    
    self = [super init];
    
    if (self) {
        _title = title;
        _coordinate = location;
    }
    return self;
}

- (MKAnnotationView *)annotationView {
    
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation: self reuseIdentifier: @"PhotoAnnotation"];
    
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType: UIButtonTypeDetailDisclosure];
    
    UIColor *fillColor = [UIColor pxColorWithHexValue: self.tintColor];
    
    CAShapeLayer *waterDrop = [self drawWaterDropShapeWithColor: fillColor];
    [annotationView.layer addSublayer:waterDrop];
    
    return annotationView;
}

- (CAShapeLayer *) drawWaterDropShapeWithColor: (UIColor *) color{
    
    CAShapeLayer *waterDropLayer = [CAShapeLayer layer];
    
    [waterDropLayer setStrokeColor: UIColor.blackColor.CGColor];
    [waterDropLayer setFillColor: color.CGColor];
    
    UIBezierPath* ovalPath = [UIBezierPath bezierPath];
    [ovalPath moveToPoint: CGPointMake(82, 41)];
    [ovalPath addCurveToPoint: CGPointMake(75.5, 57.82) controlPoint1: CGPointMake(82, 47.48) controlPoint2: CGPointMake(79.54, 53.38)];
    [ovalPath addCurveToPoint: CGPointMake(57, 73) controlPoint1: CGPointMake(70.93, 62.84) controlPoint2: CGPointMake(57, 73)];
    [ovalPath addCurveToPoint: CGPointMake(39.03, 58.38) controlPoint1: CGPointMake(57, 73) controlPoint2: CGPointMake(43.58, 63.08)];
    [ovalPath addCurveToPoint: CGPointMake(32, 41) controlPoint1: CGPointMake(34.68, 53.88) controlPoint2: CGPointMake(32, 47.75)];
    [ovalPath addCurveToPoint: CGPointMake(57, 16) controlPoint1: CGPointMake(32, 27.19) controlPoint2: CGPointMake(43.19, 16)];
    [ovalPath addCurveToPoint: CGPointMake(82, 41) controlPoint1: CGPointMake(70.81, 16) controlPoint2: CGPointMake(82, 27.19)];
    [ovalPath closePath];

    
    [waterDropLayer setPath: ovalPath.CGPath];
    
    return waterDropLayer;
}

@end
