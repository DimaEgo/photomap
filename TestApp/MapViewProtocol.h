//
//  MapViewProtocol.h
//  TestApp
//
//  Created by mac-061 on 3/14/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/NSObject.h>
#import "PhotoModel.h"

@protocol MapViewProtocol <NSObject>
-(void) showPhotoChooser;
-(void) showCurrentLocationOnMap;
-(void) showAddNewPhotoPopover: (PhotoModel *)photo;
@end
