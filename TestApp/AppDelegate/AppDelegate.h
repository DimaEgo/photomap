//
//  AppDelegate.h
//  TestApp
//
//  Created by mac-061 on 3/13/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

