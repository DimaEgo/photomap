//
//  CategoriesViewController.m
//  TestApp
//
//  Created by mac-061 on 3/15/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoriesViewController.h"
#import "CategoryModel.h"
#import "CategoryCell.h"
#import "UIColor+PXExtensions.h"
#import "TabViewController.h"
#import "AppDelegate.h"
#import "UpdateCategories.h"



@interface CategoriesViewController()
    @property (weak, nonatomic) IBOutlet UITableView *tableView;
    @property (nonatomic) NSMutableArray *categories;
@end

@implementation CategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"CategoryCell" bundle:nil] forCellReuseIdentifier:@"CategoryCell"];
    
    GetCategories *getCategories = [GetCategories new];
    _categories = [NSMutableArray new];
    [getCategories execute:^(NSMutableArray *categories) {
        _categories = categories;
        [self.tableView reloadData];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    
    CategoryModel *category = ((CategoryModel *)[_categories objectAtIndex:indexPath.row]);
    CategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
    
    UIColor *color = [UIColor pxColorWithHexValue: category.color];

    cell.labelCategoryTitle.text = [category.title uppercaseString];
    cell.labelCategoryTitle.textColor = color;
    [cell.checkboxCategory initWithColor:color];
    
    [cell.checkboxCategory setState:category.isSelected];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    CategoryModel *category = ((CategoryModel *)[_categories objectAtIndex:indexPath.row]);
    
    if (category.isSelected) {
        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    } else {
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CategoryModel *category = ((CategoryModel *)[_categories objectAtIndex:indexPath.row]);
    category.isSelected = YES;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    CategoryModel *category = ((CategoryModel *)[_categories objectAtIndex:indexPath.row]);
    category.isSelected = NO;
}

- (IBAction)buttonDoneClick:(id)sender {
    
    UpdateCategories *updateCategories = [UpdateCategories new];
    [updateCategories execute:_categories];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
