//
//  TimelineCell.m
//  TestApp
//
//  Created by L on 3/17/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TimelineCell.h"

@implementation TimelineCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
