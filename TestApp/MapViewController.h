//
//  MapViewController.h
//  TestApp
//
//  Created by mac-061 on 3/13/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController<UIGestureRecognizerDelegate, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    CLLocationManager *objLocationManager;
    double latitude_UserLocation, longitude_UserLocation;
}

@end

