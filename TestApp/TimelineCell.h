//
//  TimelineCell.h
//  TestApp
//
//  Created by L on 3/17/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagePhoto;
@property (weak, nonatomic) IBOutlet UILabel     *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel     *labelDescription;

@end
