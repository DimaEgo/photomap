//
//  CategoryCell.m
//  TestApp
//
//  Created by mac-061 on 3/15/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CategoryCell.h"

@implementation CategoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

    return self;
}

-(void)setSelected:(BOOL)selected {
    [self setSelected:selected animated:false];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self.checkboxCategory setState:selected];

}

@end
