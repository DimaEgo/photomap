//
//  Timeline.h
//  TestApp
//
//  Created by mac-061 on 3/15/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
    @property (weak, nonatomic) NSArray *photos;
@end
