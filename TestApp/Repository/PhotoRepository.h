//
//  PhotoRepository.h
//  TestApp
//
//  Created by mac-061 on 3/22/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <foundation/Foundation.h>
#import <Firebase.h>
#import "PhotoModel.h"

@interface PhotoRepository : NSObject

@property (strong, nonatomic) FIRDatabaseReference *ref;

+ (id)sharedManager;

-(void)categories: (void(^)(NSMutableArray *categories)) completion;

-(void)updateCategories:(NSArray *)categories;

-(void)addPhoto: (PhotoModel *)newPhoto;

@end
