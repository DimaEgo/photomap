//
//  PhotoRepository.m
//  TestApp
//
//  Created by mac-061 on 3/22/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import "PhotoRepository.h"
#import "CategoryModel.h"

@implementation PhotoRepository

+ (id)sharedManager {
    static PhotoRepository *repository = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
    repository = [[self alloc] init];
    repository.ref = [[FIRDatabase database] reference];
    });
    return repository;
}

- (id)init {
    if (self = [super init]) {
       
    }
    return self;
}

-(void)categories: (void(^)(NSMutableArray *categories)) completion { 
    
    NSString *userId = @"123";
    FIRDatabaseReference *refCategories = [self getCategoriesReferenceWithUserId: userId];
    
    return [refCategories observeSingleEventOfType:FIRDataEventTypeValue
    withBlock:^(FIRDataSnapshot *snapshot) {
        
        NSMutableArray * categories = [NSMutableArray new];
        
        if(snapshot.exists){
    
            for (FIRDataSnapshot* child in snapshot.children) {
                
                CategoryModel *category = [CategoryModel new];
                
                BOOL isSelected = [[child.value objectForKey:@"isSelected"] boolValue];
                
                category.title = [child.value objectForKey:@"title"];
                category.isSelected = isSelected;
                category.color = [child.value objectForKey:@"color"];
                
                [categories addObject:category];
            }
        } else {
            categories = nil;
        }
        
        if (completion) {
            completion(categories);
        }
    } withCancelBlock:nil];
    
}

-(void)updateCategories:(NSArray *)categories {
    NSString *userId = @"123";
                        
    FIRDatabaseReference *refCategories = [self getCategoriesReferenceWithUserId: userId];
    
    [refCategories observeSingleEventOfType:FIRDataEventTypeValue
        withBlock:^(FIRDataSnapshot *snapshot) {
    
        if(snapshot.exists){
            for (FIRDataSnapshot* child in snapshot.children) {
                for (CategoryModel *category in categories) {
                    if (category.title == [child.value objectForKey:@"title"]) {
                        
                        NSDictionary *post = @{@"isSelected": category.isSelected ? @"YES" : @"NO",
                                               @"color": category.color,
                                               @"title": category.title};
                        NSDictionary *childUpdates = @{[NSString stringWithFormat:@"%@", child.key]: post};
                        
                        [refCategories updateChildValues:childUpdates];
                    }
                }
            }
        }
}];
}

-(void)allPhotos: (void(^)(NSMutableArray *photos)) completion {
    NSString *userId = @"123";
    FIRDatabaseReference *refPhotos = [self getPhotosReferenceWithUserId: userId];
    
    return [refPhotos observeSingleEventOfType:FIRDataEventTypeValue
                                         withBlock:^(FIRDataSnapshot *snapshot) {
                                             
                                             NSMutableArray * photos = [NSMutableArray new];
                                             
                                             if(snapshot.exists){
                                                 
                                                 for (FIRDataSnapshot* child in snapshot.children) {
                                                     
                                                     PhotoModel *photo = [PhotoModel new];
                                                     
                                                     photo.title = [child.value objectForKey:@"title"];
                                                     photo.date = [child.value objectForKey:@"date"];
                                                     photo.latitude  = [[child.value objectForKey:@"latitude"] floatValue];
                                                     photo.longitude = [[child.value objectForKey:@"longitude"] floatValue];
                                                     photo.category = [child.value objectForKey:@"category"];
                                                     
//                                                   photo.photo = [child.value objectForKey:@"title"];
                                                     
                                                     [photos addObject:photo];
                                                 }
                                             } else {
                                                 photos = nil;
                                             }
                                             
                                             if (completion) {
                                                 completion(photos);
                                             }
                                         } withCancelBlock:nil];
}

-(void)addPhoto: (PhotoModel *)newPhoto {
    NSString *userId = @"123";
    
    FIRDatabaseReference *refPhotos = [self getPhotosReferenceWithUserId: userId];
    
    [refPhotos observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {
        if(snapshot.exists){
            
            NSDateFormatter *formatter = [NSDateFormatter new];
            [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
            [formatter setDateStyle:NSDateFormatterLongStyle];
            [formatter setTimeStyle:NSDateFormatterLongStyle];
            
            NSDictionary *post = @{@"title"    :newPhoto.title ? newPhoto.title: @"",
                                   @"date"     :[formatter stringFromDate:newPhoto.date],
                                   @"category" :newPhoto.category ? newPhoto.category: @"",
                                   @"latitude" :[NSNumber numberWithFloat:newPhoto.latitude].stringValue,
                                   @"longitude":[NSNumber numberWithFloat:newPhoto.longitude].stringValue,
                                   @"photo"    :@"nil"};
            
            [[refPhotos childByAutoId] setValue:post];
        }
    }];
}


-(FIRDatabaseReference *) getCategoriesReferenceWithUserId: (NSString *)userId{
    return [[[[_ref child:@"0"] child:@"0"] child:userId] child:@"categories"];
}

-(FIRDatabaseReference *) getPhotosReferenceWithUserId: (NSString *)userId{
    return [[[[_ref child:@"0"] child:@"0"] child:userId] child:@"photos"];
}

@end
