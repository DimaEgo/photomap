//
//  CheckboxButton.h
//  TestApp
//
//  Created by mac-061 on 3/19/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckboxButton : UIView

@property (nonatomic, readwrite) UIColor *color;
@property (nonatomic) BOOL isSelected;

-(void) initWithColor:(UIColor *) color;
-(void) changeState;
-(void)setState: (BOOL)isSelected;

@end
