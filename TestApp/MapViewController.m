//
//  ViewController.m
//  TestApp
//
//  Created by mac-061 on 3/13/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import "MapViewProtocol.h"
#import "MapViewController.h"
#import "TabViewController.h"
#import "AddPhotoViewController.h"
#import "AddPhotoDelegate.h"

#import "MapPresenter.h"
#import "PhotoAnnotaionView.h"

#import "PhotoModel.h"
#import "PhotoAnnotation.h"

#import <MobileCoreServices/UTCoreTypes.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>
#import <Photos/PHAsset.h>


@interface MapViewController ()<MapViewProtocol, UIGestureRecognizerDelegate, MKMapViewDelegate, UIPopoverPresentationControllerDelegate, AddPhotoDelegate>

@property (nonatomic, strong) MapPresenter *presenter;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton  *buttonCurrentLocation;
@property (weak, nonatomic) IBOutlet UIButton  *buttonSelectPhoto;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadUserLocation];
    [self setupGestureRecognizer];
    [self.mapView setDelegate: self];
    
    self.presenter = [[MapPresenter alloc] initWithView:self];
    
    [self getPhotos];
}

- (void)getPhotos{
    
    TabViewController *controller = ((TabViewController *)  [self tabBarController]);
    
    NSArray *photos = controller.photos;
    
    for (int i=0; i<photos.count; i++) {
        float latitude  = ((PhotoModel *)[photos objectAtIndex:i]).latitude;
        float longitude = ((PhotoModel *)[photos objectAtIndex:i]).longitude;
        
        [self showPointWithLatitude: latitude andLongtitude: longitude];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupGestureRecognizer {
    UILongPressGestureRecognizer *tapGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(didLongTapOnMap:)];
    [tapGestureRecognizer setDelegate:self];
    [self.mapView addGestureRecognizer:tapGestureRecognizer];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[MKAnnotationView class]]){
        return NO;
    }
    return YES;
}

- (void)didLongTapOnMap:(UITapGestureRecognizer *)gestureRecognizer{
    CGPoint point = [gestureRecognizer locationInView:self.mapView];
    
    [self.presenter mapLongClicked: &point];
}

- (MKAnnotationView *)mapView:()mapView viewForAnnotation:(nonnull id<MKAnnotation>)annotation{
    if ([annotation isKindOfClass:[PhotoAnnotation class]]) {
        PhotoAnnotation *photoAnnotation = (PhotoAnnotation *) annotation;
        
        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"PhotoAnnotation"];
        
        if (annotationView == nil) {
            annotationView = photoAnnotation.annotationView;
        } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
        
    } else {
        return nil;
    }
}

- (void)showPointWithLatitude: (float)latitude andLongtitude: (float)longitude {
    CGPoint point = CGPointMake(latitude, longitude);
    CLLocationCoordinate2D coordinate = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
    PhotoAnnotation *annotation = [[PhotoAnnotation alloc] init];
    annotation.coordinate = coordinate;
    annotation.tintColor = @"#578E18";
    
    [self.mapView addAnnotation: annotation];
}

- (IBAction)buttonCurrentLocationClick:(UIButton *)sender {
    [_presenter buttonCurrentLocationClicked];
}

- (IBAction)buttonChoosePhoto:(UIButton *)sender {
    [_presenter buttonChoosePhotoClicked];
}


#pragma mark - MapViewProtocol

- (void)showCurrentLocationOnMap {
    [self loadUserLocation];
}

- (void)showPhotoChooser {
    
    UIImagePickerController *imagePickerCamera =[[UIImagePickerController alloc] init];
    imagePickerCamera.delegate = self;
    imagePickerCamera.allowsEditing = YES;
    imagePickerCamera.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage,nil];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePickerCamera.cameraDevice = UIImagePickerControllerSourceTypeCamera;
    }
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]){
        imagePickerCamera.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentViewController:imagePickerCamera  animated:YES completion:nil];
}

-(void)showAddNewPhotoPopover:(PhotoModel *)photo {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    AddPhotoViewController *addPhotoController = [mainStoryboard instantiateViewControllerWithIdentifier:@"AddPhoto"];
    addPhotoController.photoModel = photo;
    addPhotoController.addPhotoDelegate = self;
    addPhotoController.modalPresentationStyle = UIModalPresentationPopover;
    addPhotoController.popoverPresentationController.delegate =  self;
    addPhotoController.popoverPresentationController.sourceView = self.view;
    addPhotoController.preferredContentSize = CGSizeMake(400, 400);

    CGRect bounds = CGRectMake(CGRectGetMidX(self.mapView.bounds), CGRectGetMidY(self.mapView.bounds), 400, 400);
    
        addPhotoController.popoverPresentationController.sourceRect = bounds;
    [self presentViewController:addPhotoController animated:true completion:nil];
}

-(UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}


#pragma mark - AddPhotoDelegate

- (void)addPhotoConfirmed: (PhotoModel *) photo{
    [_presenter addPhotoConfirmed: photo];
}

- (void) loadUserLocation{
    objLocationManager = [[CLLocationManager alloc] init];
    objLocationManager.delegate = self;
    objLocationManager.distanceFilter = kCLDistanceFilterNone;
    objLocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if ([objLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [objLocationManager requestWhenInUseAuthorization];
    }
    [objLocationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0) {
    CLLocation *newLocation = [locations objectAtIndex:0];
    latitude_UserLocation = newLocation.coordinate.latitude;
    longitude_UserLocation = newLocation.coordinate.longitude;
    [objLocationManager stopUpdatingLocation];
    [self loadMapView];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [objLocationManager stopUpdatingLocation];
}

- (void) loadMapView {
    CLLocationCoordinate2D objCoor2D = {.latitude =  latitude_UserLocation, .longitude =  longitude_UserLocation};
    MKCoordinateSpan objCoorSpan = {.latitudeDelta =  0.01, .longitudeDelta =  0.01};
    MKCoordinateRegion objMapRegion = {objCoor2D, objCoorSpan};
    [self.mapView setRegion:objMapRegion];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
        PHAsset *phAsset = [[PHAsset fetchAssetsWithALAssetURLs:@[imageURL] options:nil] lastObject];
        NSDate *date = [phAsset creationDate];
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        
        [_presenter imageSelected: image withDate: date];
    }];
}

@end
