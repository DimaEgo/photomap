//
//  CheckboxButton.m
//  TestApp
//
//  Created by mac-061 on 3/19/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import "CheckboxButton.h"

@implementation CheckboxButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
       
    }
    return self;
}

-(void)initWithColor:(UIColor *)color{
    self.color = color;
    
    self.layer.cornerRadius = 25.0f;
    self.layer.borderWidth = 2.0f;
    self.layer.borderColor = self.color.CGColor;
}

-(void)changeState {
    _isSelected = !_isSelected;
    [self setState: _isSelected];
}

-(void)setState: (BOOL)isSelected {
    if (isSelected) {
         self.backgroundColor = self.color;
    } else {
        self.backgroundColor = UIColor.whiteColor;
    }
}

@end
