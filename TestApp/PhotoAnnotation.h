//
//  PhotoAnnotation.h
//  TestApp
//
//  Created by L on 3/17/18.
//  Copyright © 2018 mac-061. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface PhotoAnnotation : NSObject <MKAnnotation>

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, readwrite) NSString *tintColor;

- (id)initWithTitle: (NSString *)title andWithLocation: (CLLocationCoordinate2D) location;
- (MKAnnotationView *) annotationView;

@end
